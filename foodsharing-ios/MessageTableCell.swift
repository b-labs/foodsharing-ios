//
//  MessageTableCell.swift
//  LoginScreenSwift
//
//  Created by Baudisgroup User on 07.04.19.
//  Copyright © 2019 com.kaleidosstudio. All rights reserved.
//

import UIKit

class MessageTableCell: UITableViewCell {

    
    @IBOutlet weak var senderLabel: UILabel!
    @IBOutlet weak var lineLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var chatImageView: UIImageView!
    
    var conversation_icon: UIImage?{
        didSet {
            DispatchQueue.main.async {
                self.chatImageView.image = self.conversation_icon!
            }
        }
    }
    // MARK: - Properties
    var conversation: Conversation? {
        didSet {
            DispatchQueue.main.async {
                guard let conversation = self.conversation else { return }
                
                self.senderLabel.text = conversation.sender
                self.lineLabel.attributedText = conversation.line
    //            let time = conversation.time!
    //            print(type(of: time))
    //            print(time)
                let date = Date(timeIntervalSince1970: TimeInterval(conversation.time!)!)
                
                let current_date = Date()
                let passed_time = current_date.timeIntervalSince(date)
                
                let dateFormatter = DateFormatter()
                dateFormatter.locale = Locale(identifier: "de_DE")
                
    //            print(passed_time)
                if passed_time < 60 {
                    self.timeLabel.text = "vor 1 Min."
                } else if passed_time < 3600 {
                    
                    self.timeLabel.text = "vor \(lround(passed_time / 60)) Min."
                    
                } else if Calendar.current.isDateInToday(date) {
                    
                    dateFormatter.dateFormat = "HH:mm"
                    self.timeLabel.text = "Heute " + dateFormatter.string(from: date)
                    
                } else if Calendar.current.isDateInYesterday(date) {
                    
                    self.timeLabel.text = "Gestern"
                    
                } else if Calendar.current.isDate(date, equalTo: Date(), toGranularity: .year) {

                    dateFormatter.dateFormat = "EEE dd.MM"
                    self.timeLabel.text = dateFormatter.string(from: date)
                } else {

                    dateFormatter.dateFormat = "dd.MM.yyyy"
                    self.timeLabel.text = dateFormatter.string(from: date)
                }
                
                if let url = conversation.image_url {
                    self.downloadImage(from: URL(string: url)!)
                } else{
                    if let sender = conversation.sender {
                        self.conversation_icon = makeIdenticon(sender)
                    }
                }
            }
        }
    }
    
    
    
    func downloadImage(from url: URL) {

        let task = URLSession.shared.dataTask(with: url, completionHandler: { data, response, error in
            guard let data = data, error == nil else {return}

            self.conversation_icon = UIImage(data: data)

        })
        task.resume()
        
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

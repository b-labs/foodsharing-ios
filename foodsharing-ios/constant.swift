//
//  constant.swift
//  foodsharing-ios
//
//  Created by Baudisgroup User on 09.04.19.
//  Copyright © 2019 com.kaleidosstudio. All rights reserved.
//

import Foundation
import UIKit

public struct Constants{
//    static let base_url = "http://127.0.0.1:18080";
    static let base_url = "https://beta.foodsharing.de";
//    static let DEFAULT_USER_PICTURE = "http://localhost:18080/img/mini_q_avatar.png"
    static let DEFAULT_USER_PICTURE = "https://beta.foodsharing.de/img/130_q_avatar.png"
    static let FoodSharing_logo_height = 39
}


struct Basket_detail {
    var description: String = ""
    var contactTypes: [Int]
    var tel = ""
    var handy = ""
    var weight: Float = 0.0
    var lifetime: Int = 0
    var lat = ""
    var lon = ""
    
}


extension String {
    /// Converts HTML string to a `NSAttributedString`
    
    var htmlAttributedString: NSAttributedString? {
        return try? NSAttributedString(data: Data(utf8), options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
    }
}

extension DateFormatter {
    var message_dateFormatter: DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter
    }
    
    var chat_dateFormatter: DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        return dateFormatter
    }


}

class MessageKitDateFormatter_German {
    
    // MARK: - Properties
    
    public static let shared = MessageKitDateFormatter_German()
    
    private let formatter = DateFormatter()
    
    
    // MARK: - Initializer
    
    private init() {
        formatter.locale = Locale(identifier: "de_DE")
    }
    
    // MARK: - Methods
    
    public func string(from date: Date) -> String {
        configureDateFormatter(for: date)
        return formatter.string(from: date)
    }
    
    public func attributedString(from date: Date, with attributes: [NSAttributedString.Key: Any]) -> NSAttributedString {
        let dateString = string(from: date)
        return NSAttributedString(string: dateString, attributes: attributes)
    }
    
    open func configureDateFormatter(for date: Date) {
        switch true {
        case Calendar.current.isDateInToday(date) || Calendar.current.isDateInYesterday(date):
            formatter.doesRelativeDateFormatting = true
            formatter.dateStyle = .short
            formatter.timeStyle = .short
        case Calendar.current.isDate(date, equalTo: Date(), toGranularity: .weekOfYear):
            formatter.dateFormat = "EEEE HH:mm"
        case Calendar.current.isDate(date, equalTo: Date(), toGranularity: .year):
            formatter.dateFormat = "E dd.MM, HH:mm"
        default:
            formatter.dateFormat = "dd.MM.yyyy, HH:mm"
        }
    }
    
}
extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
}


public struct custom_color {
    static let soil = UIColor(rgb: 0x533A1F)
    static let leaf = UIColor(rgb: 0x518E1D)
    static let incomingGray = UIColor(rgb: 0xE6E6EB)
}

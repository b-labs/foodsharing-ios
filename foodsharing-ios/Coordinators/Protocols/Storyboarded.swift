//
//  Storyboarded.swift
//  foodsharing-ios
//
//  Created by Simon Theiß on 31.05.19. Idea from https://www.hackingwithswift.com/articles/71/how-to-use-the-coordinator-pattern-in-ios-apps
//

import UIKit

// A protocol that is meant to be implemented by UIViewControllers
protocol Storyboarded {
    // returns a new instance of the object that is implementing this protocol
    static func instantiate() -> Self
}

//
extension Storyboarded where Self: UIViewController {
    // returns a new instance of the UIViewController that is implementing this protocol
    static func instantiate() -> Self {
        // this gives something like "foodsharing.LoginViewController"
        let fullName = NSStringFromClass(self)
        
        // this splits by the dot and uses everything after, giving "MyViewController"
        let className = fullName.components(separatedBy: ".")[1]
        
        // load our storyboard
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        // instantiate a view controller with that identifier, and force cast as the type that was requested
        return storyboard.instantiateViewController(withIdentifier: className) as! Self
    }
}

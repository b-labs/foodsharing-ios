//
//  MessageCoordinator.swift
//  foodsharing-ios
//
//  Created by Simon Theiß on 31.05.19.
//

import UIKit

class MessageCoordinator: ParentCoordinator {
    
    override func start() {
        let vc = MessageViewController.instantiate()
        
        // add a tabbar icon
        vc.tabBarItem = UITabBarItem(title: "Nachrichten".localized(), image: UIImage(named: "chat"), selectedImage: nil)

        // add actions to viewController
        vc.showConversation =  {
            [weak self] (conversation:Conversation) in
            self?.showConversation(conversation: conversation)
        }
        self.navigationController.pushViewController(vc, animated: false)
    }
    
    func showConversation(conversation: Conversation) {
        let vc = ChatViewController.instantiate()
        vc.conversation = conversation
        self.navigationController.pushViewController(vc, animated: true)
    }
    
}

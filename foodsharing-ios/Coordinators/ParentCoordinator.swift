//
//  FoodsharingCoordinator.swift
//  foodsharing-ios
//
//  Created by Simon Theiß on 31.05.19.
//

import UIKit

// A Coordinator implementation that has and properly handles it childCoordinators
class ParentCoordinator: NSObject, Coordinator {

    var childCoordinators = [Coordinator]()
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController = NavigationController()) {
        self.navigationController = navigationController
    }
    
    func start() {
        // do nothing this should be implemented by subclasses
    }
    
    // call this function to remove a coordinator from the list of childCoordinators
    // in the current config this method may never be called.
    func childDidFinish(_ child: Coordinator?) {
        for (index, coordinator) in childCoordinators.enumerated() {
            if coordinator === child {
                childCoordinators.remove(at: index)
                break
            }
        }
    }
}

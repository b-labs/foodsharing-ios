### Build instruction ###

1. Update Xcode

2. Install cocoapods with `sudo gem install cocoapods`.

    The current version of foodsharing-ios app depends on the following external libraries:
    
    - MessageKit
    - SocketIO
    
    They are managed by [Cocoapods](https://guides.cocoapods.org/using/getting-started.html#toc_3). You need to install this first.

    Podfile has been created in the root directory. After installation, go to project directory, run `pod install`.

3. After successfully building the libraries, open "foodsharing-ios.xcworkspace".

    A Dependency of SocketIO, Starscream, is set by SocketIO to build in Swift 5, which is not supported. So need to change in the build setting:

    - Go to left panel, select "Pods" folder.
    - Now in the main panel, you should find another left panel, or select the top left icon to open the hidden left panel.
    - Go to Build Settings -> In the left panel, From "TARGETS" choose "Starscream" -> Search "Swift" -> Navigate to "Swift Language version" -> Set to "Swift 4.2"
    - Repeat the same for the target "MessageKit"

4. Change team to your appleid, update the bundle name to: [yourappleid]-foodshareing-ios, then build.
